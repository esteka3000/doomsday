from modules.tools import *

def doomsday_finder(date):
	bonvolu = '\x1b[1;33;40m' + " Bonvolu uzi la ISO 8601 formaton: (\"YYYY-MM-DD\") " + '\x1b[0m'
	neekzistas = '\x1b[1;31;40m' + " Ĉi tiu date ne ekzistas! " + '\x1b[0m'

##LENGHT
	if len(date) < 8 :
		print(bonvolu)
		return
	elif date[-6] != '-' or date[-3] != '-' :
		print(bonvolu)
		return

##YEAR
	try:
		int(date[:-6])
		ask = False
	except:
		ask = True
	if ask :
		print(bonvolu)
		return
	
	YYYY = int(date[:-6])

##MONTH	
	if date[-5] == 0 :
		MM = date[-4:-3]
	else :
		MM = date[-5:-3]

	try:
		int(MM)
		ask = False
	except:
		ask = True
	if ask :
		print(bonvolu)
		return

	MM = int(MM)
	if MM > 12 :
		print (neekzistas)
		return

##DAY
	if date[-2] == 0 :
		DD = date[-1:]
	else :
		DD = date[-2:]

	try:
		int(DD)
		ask = False
	except:
		ask = True
	if ask :
		print(bonvolu)
		return

	DD = int(DD)
	if DD > 31 :
		print (neekzistas)
		return
	elif ( MM == 4 or MM == 6 or MM == 9 or MM == 11 ) and DD > 30 :
		print (neekzistas)
		return
	elif ( MM == 2 ) :
		if   DD > 29 :
				print (neekzistas)
				return
		elif leap(YYYY) == False and DD > 28 :
				print (neekzistas)
				return
	if YYYY < 1593 :
		print('\x1b[3;32;40m' + " Gregoria kalendaro ne ekistis… " + '\x1b[0m')
		return

	print (weekday(anchor(YYYY) - key(YYYY,MM) + DD ))