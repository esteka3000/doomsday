def leap(y):
	return (( y % 4 == 0 and y % 100 != 0 ) or y % 400 == 0)

def anchor(y):
	c = int(y / 100)
	a = [2,0,5,3][c%4]

	i = y - c * 100
	if i % 2 == 1 :
		i = i + 11
	i = i / 2
	if i % 2 == 1 :
		i = i + 11
	i = -i % 7

	return(int((a + i) % 7 ))

def key(y,m):
	if m <= 2 and leap(y):
		return[4,1][m-1]
	return [3,0,14,4,9,6,11,8,5,10,7,12][m-1]

def weekday(A):
	return ["Dimanĉo", "Lundo", "Mardo", "Merkredo", "Ĵaŭdo", "Vendredo", "Sabato"][A % 7]