import random
import time

from modules.tools import *

def ansint(ans):
	try:
		int(ans)
		ask = False
	except:
		ask = True
	if ask :
		ans = ansint(input("Eraro okasis… \n"))
	return ans

def exercise():
	YYYY = random.randint(1950 , 2030)

	MM = random.randint(1,12)

	if MM == 4 or MM == 6 or MM == 9 or MM == 11 :
		dmax = 30
	elif MM == 2 :
		if leap(YYYY) == True:
			dmax = 29
		else :
			dmax = 28
	else :
		dmax = 31

	DD = random.randint(1,dmax)

	day = weekday(anchor(YYYY)-key(YYYY,MM)+DD)

	MM = str(MM)
	if len(MM) == 1 :
		MM = "0" + MM

	DD = str(DD)
	if len(DD) == 1 :
		DD = "0" + DD

	date = (str(YYYY)+"-"+MM+"-"+DD)

	t = time.time()

	ans = ansint(input("Kiu estas la tago de ĉi tiu dato:\x1b[1;37;40m " + date + " \x1b[0m? \n"))

	t = time.time() - t
	t = int(t) + int((t-int(t))*100)/100

	if weekday(int(ans)) == day :
		print ("   t = " + str(t) + " s \n")
		return t
	else :
		print ("   Ne… " + day + " estis la ĝusta respondo…\n" )
		return "NF"

## Average of 5
def bestsofall(best,avg):
	try:
		stats = open("times/stats.txt","r").readlines()
	except:
		open("times/stats.txt","w+").write(str(3600) + "\n" + str(3600) )
		stats = open("times/stats.txt","r").readlines()

	if best < float(stats[0]) :
		stats[0] = str(best) + "\n"
		print("Gratulon! Ĉi tiu estas la nova rekordo: \x1b[1;37;45m t = " + str(best) + " s \x1b[0m!")
	if avg < float(stats[1]) :
		stats[1] = str(avg)
		print("Gratulon! Ĉi tiu estas la nova rekordo pri la averaĝo: \x1b[1;37;44m t = " + str(avg) + " s \x1b[0m!")
	open("times/stats.txt","w").write(stats[0] + stats[1] )

def A5():
	times = []
	while len(times) < 5 :
		t = exercise()
		times.append(t)
		if isinstance(t,str) :
			if times.count("NF") == 2 :
				print("\n      \x1b[1;31;40mNF…     \x1b[0m\x1b[6;31;40m -_-' \x1b[0m\n ")
				return
#ao5	
	if times.count("NF") == 1 :
		times.remove("NF")
	else :
		worst = max(times)
		times.remove(worst)

	best = min(times)
	times.remove(best)

	avg = sum(times) / len(times)
	avg = int(avg) + int((avg - int(avg)) * 100 ) / 100

	print ("        \x1b[1;33;40mGratulon!\x1b[0m\n La averaĝo estas \x1b[1;34;40m t = " + str(avg) + " s \x1b[0m ! \n La plej bona tempo estas \x1b[1;35;40m t = " + str(best) + " s \x1b[0m ! \n")

#BestsOfAll	
	bestsofall(best,avg)
		