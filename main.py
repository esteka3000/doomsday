from modules.tools import *
from modules.doomsday_finder import *
from modules.trainer import *

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--trovinto","-t", help="Trovi la tagon de dato. Bonvolu uzi la ISO 8601 formaton: (\"YYYY-MM-DD\") !")
parser.add_argument("--ekzerco","-e", help="Ekzerco pri finjuĝo.", action="store_true")
parser.add_argument("--A5","-a", help="Serio de kvin ekzerkoj pri finjuĝo, kun mezumo de la tempoj.", action="store_true")
parser.add_argument("--statistikoj","-s", help="La statistikoj pri la tempoj.", action="store_true")
parser.add_argument("--Alico", action="store_true",help=argparse.SUPPRESS)
args = parser.parse_args()
if args.trovinto:
	doomsday_finder(args.trovinto)
if args.ekzerco :
	exercise()
if args.A5 :
	A5()
if args.statistikoj:
	try:
		stats = open("times/stats.txt","r").readlines()
		print("La rekordo estas: t = " + str(float(stats[0])) + " s !\nLa rekordo pri la averaĝo estas: t = " + str(float(stats[1])) + " s !")
	except:
		print ("Ne!")
if args.Alico:
	easter = open('.none', 'r')
	print(easter.read())
	easter.close