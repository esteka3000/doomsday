# DOOMSDAY

doomsday.py computes the day of "any" date, and trains you to find it yourself, thank to the [doomsday rule](https://en.wikipedia.org/wiki/Doomsday_rule).
The code is in English, but the the program is in [Esperanto](https://en.wikipedia.org/wiki/Esperanto).

### Requirement

You need:
- [git](https://en.wikipedia.org/wiki/Git) in order to get it!
- [python 3](https://docs.python.org/3.9/) to play it!

### Getting it

Into your terminal emulator, go to the right folder and:

```
git clone https://gitlab.com/esteka3000/doomsday.git
```

### Playing with it

Then go into that _doomsday_ folder thanks to:

```
cd doomsday
```

Then simply run it with python:

```
python main.py -h
```

### Voilà!
